package docker

import (
	"fmt"
	"os"
	"regexp"

	"github.com/docker/docker/api/types"
	"github.com/sirupsen/logrus"
	gdhTypes "gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/types"
)

func (d Docker) GetContainerEndpoint(serviceName string, ep gdhTypes.EndpointSpec) (url string, found bool, err error) {
	cnts, err := d.cli.ContainerList(ctx, types.ContainerListOptions{})
	if err != nil {
		panic(err)
	}
	inPort := ep.Port
	for _, cnt := range cnts {
		m, err := regexp.MatchString(fmt.Sprintf(`.*%s((\-|\.)1)?$`, serviceName), cnt.Names[0])
		if err != nil {
			panic(err)
		}
		if m {
			if inPort == 0 && len(cnt.Ports) == 1 {
				url = fmt.Sprintf("http://localhost:%d", cnt.Ports[0].PublicPort)
				return url, true, nil
			}
			for _, port := range cnt.Ports {
				if port.PrivatePort == uint16(inPort) {
					url = fmt.Sprintf("http://localhost:%d", port.PublicPort)
					found = true
					return url, found, nil
				}
			}
		}
	}

	return
}

func (d Docker) GetEndpoint(serviceName string, ep gdhTypes.EndpointSpec) (url string, err error) {
	url, found, err := d.GetServiceEndpoint(serviceName, ep)
	if err != nil {
		panic(err)
	}
	if found {
		return
	}
	url, found, err = d.GetContainerEndpoint(serviceName, ep)
	if err != nil {
		panic(err)
	}
	if found {
		return
	}
	return "", fmt.Errorf("failed to get %s endpoint", serviceName)
}

func (d Docker) GetServiceEndpoint(serviceName string, ep gdhTypes.EndpointSpec) (url string, found bool, err error) {
	inPort := ep.Port
	logrus.Debugf("GetServiceEndpoint(%s, host:%s, port:%d, uri:%s)", serviceName, ep.Host, ep.Port, ep.URI)
	svcSpec, _, foundSrv, err := d.GetServiceSpec(serviceName)
	if err != nil {
		panic(err)
	}
	if !foundSrv {
		logrus.Debugf("No service found for: %s", serviceName)
		found = false
		return
	}
	if len(svcSpec.EndpointSpec.Ports) == 0 {
		logrus.Debugf("No ports found for service: %s", svcSpec.Name)
		os.Exit(1)
	}
	if inPort == 0 && len(svcSpec.EndpointSpec.Ports) == 1 {
		ep.Port = int(svcSpec.EndpointSpec.Ports[0].PublishedPort)
		url = ep.String()
		logrus.Debugf("Found service: %s (URL: %s)", svcSpec.Name, url)
		return url, true, nil
	}
	for _, port := range svcSpec.EndpointSpec.Ports {
		if port.TargetPort == uint32(inPort) {
			ep.Port = int(port.PublishedPort)
			url = ep.String()
			logrus.Debugf("Found service: %s (URL: %s)", svcSpec.Name, url)
			found = true
			return url, found, nil
		}
	}

	return
}
