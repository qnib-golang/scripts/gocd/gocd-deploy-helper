package docker

import (
	"context"
	"fmt"
	"strings"

	"github.com/docker/docker/client"
)

var ctx = context.Background()

type Docker struct {
	cli         *client.Client
	Mode        string
	ServiceName string
	Debug       bool
}

func NewDocker(debug bool) (d Docker, err error) {
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}
	if err != nil {
		panic(err)
	}
	d = Docker{
		cli:         cli,
		Mode:        "",
		ServiceName: "",
		Debug:       debug,
	}
	return
}

func NewDockerService(mode string, debug bool) (d Docker, err error) {
	d, err = NewDocker(debug)
	d.Mode = mode
	sl := strings.Split(mode, ":")
	if len(sl) != 2 {
		err = fmt.Errorf("docker:<name> needs a container or service name to filter for. (e.G. docker:neo4j)")
		return
	}
	d.ServiceName = sl[1]
	return
}
