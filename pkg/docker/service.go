package docker

import (
	"context"
	"fmt"
	"regexp"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/swarm"
	"github.com/sirupsen/logrus"
)

func (d Docker) GetServiceSpec(serviceName string) (res swarm.ServiceSpec, version swarm.Version, found bool, err error) {
	logrus.Debugf("GetServiceSpec(%s)", serviceName)
	svc, found, err := d.GetService(serviceName)
	if err != nil {
		return
	}
	if found {
		res = svc.Spec
		version = svc.Version
		found = true
		return
	}

	return
}
func (d Docker) GetService(serviceName string) (res swarm.Service, found bool, err error) {
	logrus.Debugf("GetService(%s)", serviceName)
	opts := types.ServiceListOptions{}
	svcs, err := d.cli.ServiceList(ctx, opts)
	if err != nil {
		panic(err)
	}

	for _, svc := range svcs {
		m, e := regexp.MatchString(fmt.Sprintf(`.*%s$`, serviceName), svc.Spec.Name)
		if e != nil {
			panic(e)
		}
		if m {
			res = svc
			found = true
			return
		}
	}
	return
}

func (d Docker) RestartService() (err error) {
	svc, foundSrv, err := d.GetService(d.ServiceName)
	if err != nil {
		return err
	}
	if !foundSrv {
		return fmt.Errorf("service %s not found", d.ServiceName)
	}
	svc.Spec.TaskTemplate.ForceUpdate++
	resp, err := d.cli.ServiceUpdate(ctx, svc.ID, swarm.Version{Index: svc.Version.Index}, svc.Spec, types.ServiceUpdateOptions{})
	if err != nil {
		return err
	}
	logrus.Debugf("RestartService(%s) -> %s", d.ServiceName, resp)
	return
}

func (d Docker) GetContainerName(serviceName string) (cntName string, found bool, err error) {
	logrus.Debugf("GetContainer(%s)", serviceName)
	svcSpec, _, foundSrv, err := d.GetServiceSpec(serviceName)
	if err != nil {
		panic(err)
	}
	if foundSrv {
		filters := filters.NewArgs()
		filters.Add("service", svcSpec.Name)
		filters.Add("desired-state", "running")
		opts := types.TaskListOptions{
			Filters: filters,
		}
		tasks, e := d.cli.TaskList(ctx, opts)
		if e != nil {
			panic(e)
		}
		for _, task := range tasks {
			cntName = fmt.Sprintf("%s.%d.%s", svcSpec.Name, task.Slot, task.ID)
			found = true
			return
		}

		found = true
		return
	}
	return
}
func (d Docker) GetContainerID(cntName string) (cntID string, found bool, err error) {
	containers, err := d.cli.ContainerList(context.Background(), types.ContainerListOptions{})
	if err != nil {
		panic(err)
	}
	r := fmt.Sprintf(`/%s$`, cntName)
	logrus.Debugf("GetContainerID(%s) -> %s", cntName, r)
	for _, container := range containers {
		for _, name := range container.Names {
			m, e := regexp.MatchString(r, name)
			if e != nil {
				panic(e)
			}
			if m {
				cntID = strings.TrimLeft(name, "/")
				found = true
				return
			}

		}
	}
	return
}
