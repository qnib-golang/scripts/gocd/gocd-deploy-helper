package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/files"
)

var (
	restartFlag bool
)
var fileHashCmd = &cobra.Command{
	Use:   "files [flags] docker:<name> ...<local_file_path>:<container_hash_path>",
	Short: "Compares the sha256 hash of a local file with the hash within a container",
	Run:   files.RunFileHashCheck,
}

func init() {
	rootCmd.AddCommand(fileHashCmd)
	fileHashCmd.PersistentFlags().BoolVar(&debugFlag, "debug", false, "Prints debug output")
	fileHashCmd.PersistentFlags().BoolVar(&restartFlag, "restart", false, "When hash does not match restart the service and check again after the service is healthy")
	fileHashCmd.PersistentFlags().IntVar(&timeoutSec, "timeout", 15, "Timeout for the health check in seconds")

}
