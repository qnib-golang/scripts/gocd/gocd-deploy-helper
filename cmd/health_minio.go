package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/minio"
)

var minioHealthCmd = &cobra.Command{
	Use:   "minio",
	Short: "Checks a MinIO instance",
	Long: `Checks a MinIO instance in two modes:
- docker: uses the docker API. takes the service or container name as an argument
- http: uses the http API. takes the url as an argument
	`,
	Run: minio.CheckHealth,
}

func init() {
	httpHealthCmd.AddCommand(minioHealthCmd)
	minioHealthCmd.Flags().StringVar(&mode, "mode", "", "docker (argument: regex) mode or http (http[s]:<addr>:<port>) mode?")
}
