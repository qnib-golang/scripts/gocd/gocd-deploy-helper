package files

import (
	"bytes"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/docker"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/helper"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/lib/tools"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/neo4j"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/types"
)

func RunFileHashCheck(cmd *cobra.Command, args []string) {
	helper.InitLogging(cmd)
	timeout, _ := cmd.Flags().GetInt("timeout")
	debug, _ := cmd.Flags().GetBool("debug")
	restart, _ := cmd.Flags().GetBool("restart")
	logrus.Debugf("RunFileHashCheck: %v", args)
	files := types.NewFiles()
	if len(args) >= 2 {
		logrus.Debugf("args: %v", args)
		mode := args[0]
		for _, arg := range args[1:] {
			logrus.Debugf("arg: %s", arg)
			parts := strings.Split(arg, ":")
			if len(parts) != 2 {
				logrus.Errorf("Invalid argument: %s", arg)
				os.Exit(1)
			}
			files.AddFile(types.NewFileFromParts(parts))
		}

		d, err := docker.NewDockerService(mode, debug)
		if err != nil {
			msg := "failed to get create docker client"
			logrus.Error(msg)
			os.Exit(1)
		}
		res, err := CheckFileHash(d, files)
		if err != nil {
			msg := fmt.Sprintf("Failed to check file hash: %v", err.Error())
			logrus.Error(msg)
			os.Exit(1)
		}
		if !res.Equal {
			uEq := res.GetUnequal()
			for _, u := range uEq {
				msg := fmt.Sprintf("Files are not equal: %s != %s", u.Local, u.Cnt)
				if restart {
					logrus.Warn(msg)
				} else {
					logrus.Error(msg)
				}
			}
			if !restart {
				os.Exit(1)
			}

			err = d.RestartService()
			if err != nil {
				err = errors.Wrapf(err, "failed to restart service %s", d.ServiceName)
				logrus.Error(err)
				os.Exit(1)
			}
			to := 5
			logrus.Debugf("Wait %ds for the updarte to have an effect", to)
			time.Sleep(time.Duration(to) * time.Second)
			// TODO: wait for the service to be up again
			healthy, err := neo4j.RunHealthCheck(d, timeout)
			if err != nil {
				err = errors.Wrapf(err, "failed to run health check for service %s", d.ServiceName)
				logrus.Error(err)
				os.Exit(1)
			}
			if !healthy {
				err = fmt.Errorf("service %s is not healthy", d.ServiceName)
				logrus.Error(err)
				os.Exit(1)
			}
			// TODO: run CheckFileHash again
			res, err := CheckFileHash(d, files)
			if err != nil {
				msg := fmt.Sprintf("Failed to check file hash: %v", err.Error())
				logrus.Error(msg)
				os.Exit(1)
			}
			if !res.Equal {
				uEq := res.GetUnequal()
				for _, u := range uEq {
					msg := fmt.Sprintf("Files are not equal after restart: %s != %s", u.Local, u.Cnt)
					logrus.Error(msg)
				}
				os.Exit(1)
			}
			logrus.Info("Files are equal after restart")
			os.Exit(0)
		}
	}
}

func CheckFileHash(d docker.Docker, files types.Files) (out types.Files, err error) {
	logrus.Debugf(" > CheckFileHash(%v)", files.Data)
	switch {
	case strings.HasPrefix(d.Mode, "docker:"):
		//cntName, found, err := d.GetContainerName(name)
		cntID, found, err := d.GetContainerName(d.ServiceName)
		if err != nil {
			err = errors.Wrap(err, "failed to get service")
			return files, err
		}
		if !found {
			err = fmt.Errorf("failed to find container for service '%s\n'", d.ServiceName)
			return files, err
		}
		out.Equal = true
		for _, file := range files.Data {
			cntHash, err := d.GetFileContentFromContainer(cntID, file.Cnt)
			if err != nil {
				err = errors.Wrap(err, "failed to get hash from container")
				return files, err
			}
			b, err := os.ReadFile(file.Local) // just pass the file name
			if err != nil {
				err = errors.Wrap(err, "failed to read local file")
				return files, err
			}
			localBuf := bytes.NewBuffer(b)
			localHash, err := tools.CreateHashFromFileBuf(localBuf, "sha256")
			if err != nil {
				err = errors.Wrap(err, "failed to get hash from local file")
				return files, err
			}
			if cntHash == localHash {
				msg := fmt.Sprintf("Hash of %s in container %s matches local file %s\n", file.Cnt, cntID, file.Local)
				logrus.Debugf(msg)
				file.Equal = true
			} else {
				msg := fmt.Errorf("hash of %s in container %s does not match local file %s", file.Cnt, cntID, file.Local)
				logrus.Warn(msg)
				logrus.Debugf("cnt: '%s' != '%s' :local", cntHash, localHash)
				out.Equal = false
			}
		}
	}
	return out, err
}
