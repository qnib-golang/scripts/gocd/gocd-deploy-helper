package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/neo4j"
)

var userFlag, passwdFlag, mode string

var neo4jHealthCmd = &cobra.Command{
	Use:   "neo4j",
	Short: "Checks a NEO4J instance",
	Long: `Checks a NEO4J instance in two modes:
- docker: uses the docker API. takes the service or container name as an argument
- http: uses the http API. takes the url as an argument
	`,
	Run: neo4j.CheckHealth,
}

func init() {
	httpHealthCmd.AddCommand(neo4jHealthCmd)
	neo4jHealthCmd.Flags().StringVar(&mode, "mode", "docker", "docker (argument: regex) mode or http (http[s]:<addr>:<port>) mode?")
	neo4jHealthCmd.Flags().StringVar(&userFlag, "user", "neo4j", "neo4j user")
	neo4jHealthCmd.Flags().StringVar(&passwdFlag, "password", "test", "neo4j password")

}
