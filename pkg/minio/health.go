package minio

import (
	"fmt"
	"os"
	"strings"

	"github.com/bitfield/script"
	"github.com/spf13/cobra"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/docker"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/helper"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/types"
)

func CheckHealth(cmd *cobra.Command, args []string) {
	helper.InitLogging(cmd)
	timeoutSec, _ := cmd.Flags().GetInt("timeout")
	debug, _ := cmd.Flags().GetBool("debug")
	var url string
	mode, _ := cmd.Flags().GetString("mode")
	name, _ := cmd.Flags().GetString("name")
	ep := types.NewEndpointSpec(9000)
	switch {
	case strings.HasPrefix(mode, "docker"):
		d, err := docker.NewDocker(debug)
		if err != nil {
			msg := fmt.Sprintf("Failed to get create docker client: %v", err.Error())
			script.Echo(msg).Stdout()
		}
		url, err = d.GetEndpoint(name, ep)
		if err != nil {
			msg := fmt.Sprintf("Failed to get container endpoint: %v", err.Error())
			script.Echo(msg).Stdout()
		}

	case strings.HasPrefix(mode, "http"):
		url = args[0]
	default:
		script.Echo("Unknown mode: docker and http.*").Stdout()
	}
	res, err := helper.CheckHealthURL(url, timeoutSec, debug)
	if err != nil {
		script.Echo(err.Error() + "\n").Stdout()
		os.Exit(1)
	}
	script.Echo(res).Stdout()

}
