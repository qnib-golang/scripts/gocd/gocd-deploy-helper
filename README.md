# GOCD Deploy Helper (gdh)

## Health

Let's start a nginx service to check.

```bash
docker service create --name http -p 8888:80  nginx:stable
```

Once the service is up, you can check the service:

```bash
gocd-deploy-helper health --port=80 docker:http
```
