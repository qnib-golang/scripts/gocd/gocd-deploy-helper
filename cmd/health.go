package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/helper"
)

var urlFlag, uriPath string
var timeoutSec, inPort int
var debugFlag bool

var httpHealthCmd = &cobra.Command{
	Use:   "health [flags] (docker:<name> | http[s]:<addr>:<port>)[/<path>]",
	Short: "Fetches enpoint via docker API and checks for status code 200",
	Run:   helper.CheckHealth,
}

func init() {
	rootCmd.AddCommand(httpHealthCmd)
	httpHealthCmd.Flags().StringVar(&urlFlag, "url", "http://localhost:8080", "Which endpoint to check")
	httpHealthCmd.Flags().StringVar(&uriPath, "uri", "", "Which path to check")
	httpHealthCmd.PersistentFlags().IntVar(&timeoutSec, "timeout", 10, "Timeout in seconds")
	httpHealthCmd.PersistentFlags().IntVar(&inPort, "port", 0, "internal port of container to check for")
	httpHealthCmd.PersistentFlags().BoolVar(&debugFlag, "debug", false, "Prints debug output")

}
