package helper

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

func InitLogging(cmd *cobra.Command) {
	debug, _ := cmd.Flags().GetBool("debug")
	if debug {
		logrus.SetLevel(logrus.DebugLevel)
	}
}
