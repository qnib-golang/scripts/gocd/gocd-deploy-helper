package types

type File struct {
	Equal bool
	Local string
	Cnt   string
}

type Files struct {
	Equal bool
	Data  []File
}

func NewFiles() Files {
	return Files{
		Equal: false,
		Data:  make([]File, 0),
	}
}

func (f *Files) AddFile(file File) {
	f.Data = append(f.Data, file)
}

func NewFileFromParts(parts []string) File {
	return File{
		Equal: false,
		Local: parts[0],
		Cnt:   parts[1],
	}
}

func (f *Files) GetUnequal() []File {
	out := make([]File, 0)
	for _, file := range f.Data {
		if !file.Equal {
			out = append(out, file)
		}
	}
	return out
}
