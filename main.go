/*
Copyright © 2023 Christian Kniep <christian@qnib.org>
*/
package main

import "gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/cmd"

func main() {
	cmd.Execute()
}
