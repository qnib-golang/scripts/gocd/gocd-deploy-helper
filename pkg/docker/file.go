package docker

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"strings"

	"archive/tar"

	"github.com/sirupsen/logrus"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/lib/tools"
)

func (d Docker) GetFileContentFromContainer(containerID, cntPath string) (cont string, err error) {
	buf, err := d.GetFileBufFromContainer(containerID, cntPath)
	if err != nil {
		return
	}
	cont = buf.String()
	cont = strings.TrimSpace(cont)
	return
}

func (d Docker) GetFileHashFromContainer(containerID, alg, cntPath string) (h string, err error) {
	buf, err := d.GetFileBufFromContainer(containerID, cntPath)
	if err != nil {
		return "", err
	}
	return tools.CreateHashFromFileBuf(buf, alg)
}

func (d Docker) GetFileBufFromContainer(containerID, cntPath string) (b *bytes.Buffer, err error) {
	logrus.Debugf("GetFileFromContainer(%s, cntPath:%s)", containerID, cntPath)
	reader, _, err := d.cli.CopyFromContainer(ctx, containerID, cntPath)
	if err != nil {
		msg := fmt.Sprintf("Failed to copy from container: %v", err.Error())
		logrus.Error(msg)
		return
	}
	tr := tar.NewReader(reader)
	buf := new(bytes.Buffer)
	for {
		// hdr gives you the header of the tar file
		_, err := tr.Next()
		if err == io.EOF {
			// end of tar archive
			break
		}
		if err != nil {
			log.Fatalln(err)
		}
		buf.ReadFrom(tr)

	}
	return buf, nil
}
