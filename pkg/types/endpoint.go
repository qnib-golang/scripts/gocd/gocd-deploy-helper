package types

import (
	"fmt"
	"strings"
)

type EndpointSpec struct {
	Host string
	Port int
	URI  string
}

func NewEndpointSpec(port int) EndpointSpec {
	return EndpointSpec{
		Host: "localhost",
		URI:  "",
		Port: port,
	}
}

func (ep EndpointSpec) String() string {
	uri := strings.TrimLeft(ep.URI, "/")
	return fmt.Sprintf("http://%s:%d/%s", ep.Host, ep.Port, uri)
}
