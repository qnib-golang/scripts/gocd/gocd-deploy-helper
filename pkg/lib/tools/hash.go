package tools

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"hash"
	"strings"

	"github.com/sirupsen/logrus"
)

func CreateHashFromFileBuf(buf *bytes.Buffer, alg string) (h string, err error) {
	newReader := bytes.NewReader(buf.Bytes())
	newBuf := new(bytes.Buffer)
	newBuf.ReadFrom(newReader)
	var hasher hash.Hash
	switch alg {
	case "sha256":
		hasher = sha256.New()
	default:
		logrus.Fatalf("Unknown algorithm: %s", alg)
	}
	_, err = hasher.Write(buf.Bytes())
	if err != nil {
		logrus.Fatal(err)
	}
	h = fmt.Sprintf("%x", hasher.Sum(nil))
	h = strings.TrimSpace(h)
	return
}
