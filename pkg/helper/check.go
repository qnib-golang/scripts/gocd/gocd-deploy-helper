package helper

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/bitfield/script"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/docker"
	"gitlab.com/qnib-golang/scripts/gocd/gocd-deploy-helper/pkg/types"
)

func CheckHealth(cmd *cobra.Command, args []string) {
	debug, _ := cmd.Flags().GetBool("debug")
	InitLogging(cmd)
	to, _ := cmd.Flags().GetInt("timeout")
	inPort, _ := cmd.Flags().GetInt("port")
	uriPath, _ := cmd.Flags().GetString("uri")
	ep := types.EndpointSpec{
		Host: "localhost",
		Port: inPort,
		URI:  uriPath,
	}
	logrus.Debugf("timeout: %d", to)
	logrus.Debugf("port: %d", inPort)
	var url string
	if len(args) == 1 {
		a0 := args[0]
		switch {
		case strings.HasPrefix(a0, "docker:"):
			sl := strings.Split(a0, ":")
			if len(sl) != 2 {
				script.Echo("docker:<name> needs a container or service name to filter for. (e.G. docker:neo4j)\n").Stdout()
				os.Exit(1)
			}
			name := sl[1]
			d, err := docker.NewDocker(debug)
			if err != nil {
				msg := fmt.Sprintf("Failed to get create docker client: %v", err.Error())
				script.Echo(msg).Stdout()
				os.Exit(1)
			}
			url, err = d.GetEndpoint(name, ep)
			if err != nil {
				msg := fmt.Sprintf("Failed to get container endpoint: %v", err.Error())
				script.Echo(msg).Stdout()
				os.Exit(1)
			}
		}
	} else {
		url, _ = cmd.Flags().GetString("url")
	}

	r, err := CheckHealthURL(url, to, debug)
	if err != nil {
		script.Echo(err.Error() + "\n").Stdout()
		os.Exit(1)
	}
	script.Echo(r).Stdout()
}

func CheckHealthURL(url string, sec int, debug bool) (r string, err error) {
	reader := strings.NewReader(``)
	logrus.Debugf("CheckHealth: %s", url)
	request, err := http.NewRequest("GET", url, reader)
	if err != nil {
		return "", err
	}
	client := &http.Client{}
	for i := 0; i < sec; i++ {
		resp, err := client.Do(request)
		switch {
		case err == nil && resp.StatusCode == 200:
			// Success
			return resp.Status + "\n", nil
		default:
			// we have not timed out yet
			time.Sleep(1 * time.Second)
			continue
		}
	}
	return "", fmt.Errorf("timeout after %d seconds", sec)
}
